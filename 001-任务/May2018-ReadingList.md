
* Source: http://jfe.rochester.edu/astat.pdf

- Franklin H. Allen; Yiming Qian; Guoqian Tu and Frank Yu, Entrusted loans: A close look at China’s shadow banking system, JFE, forthcoming

- Giovanna Nicodano and Luca Regis,
A trade-off theory of ownership and capital structure, JFE, forthcoming

#### Managers Traits
- (*) John (Jianqiu) Bai; Linlin Ma; Kevin Mullally and David Solomon, 
What a difference a (birth) month makes: The relative age effect and fund manager performance, JFE, forthcoming

- (*) Renee B. Adams; Matti Keloharju and Samuli Knupfer, Are CEOs born leaders? Lessons from traits of a million individuals, JFE, forthcoming

- Renee B. Adams; Ali Akyol and Patrick Verwijmeren, Director skill sets

#### Innovation
- Xin Chang; Yangyang Chen; Sarah Qian Wang; Kuo Zhang and Wenrui Zhang, Credit default swaps and corporate innovation, JFE, forthcoming

- William Mann, Creditor rights and innovation: Evidence from patent collateral, JFE, forthcoming

- Mariano M. Croce; Thien T. Nguyen; S. Raymond and Lukas M. Schmid, Government debt and the returns to innovation

- Carola Frydman and Dimitris Papanikolaou, In search of ideas: Technological innovation and executive pay inequality

- Rogier M. Hanselaar; René M. Stulz and Mathijs A. van Dijk, Do firms issue more equity when markets become more liquid? 

### Corporate Governance and Corperate Finance

- Reena Aggarwal; Sandeep Dahiya and Nagpurnanand R. Prabhala, The power of shareholder votes: Evidence from uncontested director elections

- Kyeong Hun Lee; David C. Mauer and Qianying (Emma) Xu, Human capital relatedness and mergers and acquisitions

- Will Gornall and Ilya A. Strebulaev, Financing as a supply chain: The capital structure of banks and borrowers

- S. Azizpour; Kay Giesecke and Gustavo Schwenkler, Exploring the sources of default clustering




- Christian Badarinza and Tarun Ramadorai, Home away from home? Foreign demand and London house prices

