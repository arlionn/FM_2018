use laeven_levine_jfe.dta, clear

**************************************************
***                                            ***  
***               Tables 1 and 2               ***
***              Summary statistics            ***
***                                            ***  
**************************************************

** Bank-level variables **
summ lz3 roa_sd vol3yr01 earning_sd cf sharemgt mgr totrevgr1 mshare nyse size llprat liqrat if srights~=. & lz3~=.

** Country-level variables **
preserve
collapse lz3 lgdppc95 srights min_car res_cap restrict di enforce ma_volume gdpgrvol if srights~=. & lz3~=., by(ctyname) 
summ lgdppc95 srights min_car res_cap restrict di enforce ma_volume gdpgrvol if srights~=. & lz3~=.
restore

** Correlation matrix **
pwcorr lz3 vol3yr01 earning_sd cf totrevgr1 sharemgt mgr lgdppc95 srights min_car res_cap restrict di if srights~=. & lz3~=., sig

tabstat lz3 vol3yr01 earning_sd cf totrevgr1 sharemgt mgr lgdppc95 srights min_car res_cap restrict di if srights~=. & lz3~=., by(ctyname) stats(mean)
tabstat lz3 vol3yr01 earning_sd cf totrevgr1 sharemgt mgr lgdppc95 srights min_car res_cap restrict di if srights~=. & lz3~=., by(ctyname) stats(n)

** Appendix table : Ownership data **
tabstat lz3 vol3yr01 earning_sd cf sharemgt mgr srights min_car res_cap restrict di if srights~=. & lz3~=., by(ctyname) stats(mean)
tabstat lz3 if srights~=. & lz3~=., by(ctyname) stats(n)

********************************************************************
***                                                              ***
***                     Table 3                                  ***
*** Ownership, bank supervision and bank stability               ***
***                                                              ***
********************************************************************

** OLS with robust standard errors and clustering at the country-level **

** Table 3: OLS **
reg lz3 totrevgr1 cf if year==2001, robust cluster(ctyno)
outreg using out1.doc, nolabel coefastr se 3aster replace
** Country fixed effects *
xtreg lz3 totrevgr1 cf if year==2001, i(ctyno) fe robust cluster(ctyno)
outreg using out1.doc, nolabel coefastr se 3aster append
*** Per capita income ***
reg lz3 totrevgr1 cf lgdppc95 if year==2001, robust cluster(ctyno)
outreg using out1.doc, nolabel coefastr se 3aster append
** Instrumental variables ** 
* Average CF of other banks is a good instrument *
* Exclude countries with only one bank *
ivreg2 lz3 totrevgr1 (cf=cfother) lgdppc95 if year==2001, robust cluster(ctyno) first
outreg using out1.doc, nolabel coefastr se 3aster append
** Hausman test for IV (Hausman cannot be used with robust/cluster option) **
ivreg2 lz3 totrevgr1 (cf=cfother) lgdppc95 if year==2001, robust
est store iv
reg lz3 totrevgr1 cf lgdppc95 if year==2001, robust
hausman iv ., sigmamore

* Alternative risk measures *
* Equity volatility
reg vol3yr01 totrevgr1 cf lgdppc95 if year==2001, robust cluster(ctyno)
outreg using out1.doc, nolabel coefastr se 3aster append
* Earnings volatility
reg earning_sd totrevgr1 cf lgdppc95 if year==2001, robust cluster(ctyno)
outreg using out1.doc, nolabel coefastr se 3aster append

** Shareholder rights and regulations (capital requirements, capital restrictions, activity restrictions, explicit deposit insurance) **
reg lz3 totrevgr1 cf lgdppc95 srights min_car res_cap restrict di if year==2001, robust cluster(ctyno)
outreg using out1.doc, nolabel coefastr se 3aster append

* Other country and bank-level controls *
* Incl. merger activity using completed deals data from Rossi and Volpin, JFE 2004 **
reg lz3 totrevgr1 cf lgdppc95 srights min_car res_cap restrict di enforce conc5 ma_volume size llprat liqrat if year==2001, robust cluster(ctyno)
outreg using out1.doc, nolabel coefastr se 3aster append
* Plus large owner on mgt board and managerial ownership variables **
reg lz3 totrevgr1 cf lgdppc95 srights min_car res_cap restrict di enforce conc5 ma_volume size llprat liqrat sharemgt mgr if year==2001, robust cluster(ctyno)
outreg using out1.doc, nolabel coefastr se 3aster append

****************************************************************
***                                                          ***
***                     Table 4                              ***
***  Interactions between ownership and banking regulation   ***
***                                                          ***
****************************************************************

** Table: Interactions **
reg lz3 totrevgr1 cf lgdppc95 srights min_car res_cap restrict di cf_car if year==2001, robust cluster(ctyno)
outreg using out3.doc, nolabel coefastr se 3aster replace
reg lz3 totrevgr1 cf lgdppc95 srights min_car res_cap restrict di cf_cap if year==2001, robust cluster(ctyno)
outreg using out3.doc, nolabel coefastr se 3aster append
reg lz3 totrevgr1 cf lgdppc95 srights min_car res_cap restrict di cf_res if year==2001, robust cluster(ctyno)
outreg using out3.doc, nolabel coefastr se 3aster append
reg lz3 totrevgr1 cf lgdppc95 srights min_car res_cap restrict di cf_di if year==2001, robust cluster(ctyno)
outreg using out3.doc, nolabel coefastr se 3aster append
reg lz3 totrevgr1 cf lgdppc95 srights min_car res_cap restrict di cf_car cf_cap cf_res cf_di if year==2001, robust cluster(ctyno)
outreg using out3.doc, nolabel coefastr se 3aster append
** Asset risk **
reg roa_sd totrevgr1 cf lgdppc95 srights min_car res_cap restrict di cf_car cf_cap cf_res cf_di if year==2001 & lz3~=., robust cluster(ctyno)
outreg using out3.doc, nolabel coefastr se 3aster append
* Reverse leverage **
reg car_avg totrevgr1 cf lgdppc95 srights min_car res_cap restrict di cf_car cf_cap cf_res cf_di if year==2001 & lz3~=., robust cluster(ctyno)
outreg using out3.doc, nolabel coefastr se 3aster append


****************************************************************
***                                                          ***
***                     Table 5                              ***
***                 IV with Tobin's Q                        ***
***                                                          ***
****************************************************************

ivreg2 lz3 (q1=mshare nyse entrytest) totrevgr1 cf lgdppc95 srights min_car res_cap restrict di if year==2001, robust cluster(ctyno) first
outreg using out4.doc, nolabel coefastr se 3aster replace
* with interactions*
ivreg2 lz3 (q1=mshare nyse entrytest) totrevgr1 cf lgdppc95 srights min_car res_cap restrict di cf_car cf_cap if year==2001, robust cluster(ctyno) first
outreg using out4.doc, nolabel coefastr se 3aster append
ivreg2 lz3 (q1=mshare nyse entrytest) totrevgr1 cf lgdppc95 srights min_car res_cap restrict di cf_car cf_cap cf_res cf_di if year==2001, robust cluster(ctyno) first
outreg using out4.doc, nolabel coefastr se 3aster append
ivreg2 roa_sd (q1=mshare nyse entrytest) totrevgr1 cf lgdppc95 srights min_car res_cap restrict di cf_car cf_cap cf_res cf_di if year==2001 & lz3~=., robust cluster(ctyno) first
outreg using out4.doc, nolabel coefastr se 3aster append

****************************************************************
***                                                          ***
***                     Table 6       (add to table 3)       *** 
***             Alternative stability measures               ***
***                                                          ***
****************************************************************




